
import 'dart:io';
import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_ip/get_ip.dart';
import 'package:hive/hive.dart';
import 'package:last_minute_hotels_huawei/localization/locale.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/web_view2.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/webview.dart';
import 'package:last_minute_hotels_huawei/src/ui/widgets/calender.dart';
import 'package:last_minute_hotels_huawei/src/ui/widgets/guest_bottom_sheet.dart';
import 'package:last_minute_hotels_huawei/src/ui/widgets/guest_sheet.dart';
import 'package:last_minute_hotels_huawei/src/ui/widgets/home_drawer.dart';
import 'package:last_minute_hotels_huawei/src/ui/widgets/home_page_field_box.dart';
import 'package:last_minute_hotels_huawei/src/utils/constants.dart';
import 'package:last_minute_hotels_huawei/src/utils/object_factory.dart';
import 'package:last_minute_hotels_huawei/src/utils/utils.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wifi/wifi.dart';
import 'package:data_connection_checker/data_connection_checker.dart';


import 'destination_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  var destination = "Select destination";
  String dates;
  String date;
  String day1;
  String day2;
  String night;
  String monthInLetter;
  String guest;
  String checkInDate;
  String checkOutDate;
  String nights;
  String adultLabel;
  String roomLabel;
  String childLabel;
  String ip = "";
  int rooms = 0;
  int adults = 0;
  int children = 0;
  int counterValueAdults1=2;
  int counterValueAdults2=0;
  int counterValueAdults3=0;
  int counterValueAdults4=0;
  int counterValueAdults5=0;
  int counterValueAdults6=0;
  int counterValueAdults7=0;
  int counterValueAdults8=0;
  int counterValueAdults9=0;
  int counterValueAdultsSum=0;

  int counterValueChildren1=0;
  int counterValueChildren2=0;
  int counterValueChildren3=0;
  int counterValueChildren4=0;
  int counterValueChildren5=0;
  int counterValueChildren6=0;
  int counterValueChildren7=0;
  int counterValueChildren8=0;
  int counterValueChildren9=0;
  int counterValueChildrenSum=0;
  List<int> adultArray;
  List<int> childrenArray;
  SharedPreferences _sharedPreferences;
  String monthInLetterCheckIn;
  String monthInLetterCheckOut;
  AnimationController animationController;
  Animation animationSize;
  String defaultLang = Platform.localeName;
  void getSharedPref() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }


  @override
  void initState() {
    print(defaultLang);
    // getIp();
    // getPublicIP();
    if(Hive.box('adult').get(1)==null)
      Hive.box('adult').put(1, 1);
    counterValueAdults1=Hive.box('adult').get(21) != null?Hive.box('adult').get(21):counterValueAdults1;
    counterValueAdults2=Hive.box('adult').get(21) != null?Hive.box('adult').get(22):counterValueAdults2;
    counterValueAdults3=Hive.box('adult').get(21) != null?Hive.box('adult').get(23):counterValueAdults3;
    counterValueAdults4=Hive.box('adult').get(21) != null?Hive.box('adult').get(24):counterValueAdults4;
    counterValueAdults5=Hive.box('adult').get(21) != null?Hive.box('adult').get(25):counterValueAdults5;
    counterValueAdults6=Hive.box('adult').get(21) != null?Hive.box('adult').get(26):counterValueAdults6;
    counterValueAdults7=Hive.box('adult').get(21) != null?Hive.box('adult').get(27):counterValueAdults7;
    counterValueAdults8=Hive.box('adult').get(21) != null?Hive.box('adult').get(28):counterValueAdults8;
    counterValueAdults9=Hive.box('adult').get(21) != null?Hive.box('adult').get(29):counterValueAdults9;

    counterValueAdultsSum=counterValueAdults1+counterValueAdults2+counterValueAdults3+counterValueAdults4+counterValueAdults5+counterValueAdults6+counterValueAdults7+counterValueAdults8+counterValueAdults9;
    adultLabel=counterValueAdultsSum>1?"Adults":"Adult";

    counterValueChildren1=Hive.box('adult').get(31) != null?Hive.box('adult').get(31):counterValueChildren1;
    counterValueChildren2=Hive.box('adult').get(32) != null?Hive.box('adult').get(32):counterValueChildren2;
    counterValueChildren3=Hive.box('adult').get(33) != null?Hive.box('adult').get(33):counterValueChildren3;
    counterValueChildren4=Hive.box('adult').get(34) != null?Hive.box('adult').get(34):counterValueChildren4;
    counterValueChildren5=Hive.box('adult').get(35) != null?Hive.box('adult').get(35):counterValueChildren5;
    counterValueChildren6=Hive.box('adult').get(36) != null?Hive.box('adult').get(36):counterValueChildren6;
    counterValueChildren7=Hive.box('adult').get(37) != null?Hive.box('adult').get(37):counterValueChildren7;
    counterValueChildren8=Hive.box('adult').get(38) != null?Hive.box('adult').get(38):counterValueChildren8;
    counterValueChildren9=Hive.box('adult').get(39) != null?Hive.box('adult').get(39):counterValueChildren9;

    counterValueChildrenSum=counterValueChildren1+counterValueChildren2+counterValueChildren3+counterValueChildren4+counterValueChildren5+counterValueChildren6+counterValueChildren7+counterValueChildren8+counterValueChildren9;
    childLabel=counterValueChildrenSum>1?"Children":"child";

    Intl.defaultLocale = Hive.box('locale').get(1) !=null ? Locale(Hive.box('locale').get(2),Hive.box('locale').get(1)).toString() : defaultLang;
    var now = new DateTime.now();
    var formatter = new DateFormat('dd MMM');
    day2 = formatter.format(now.add(Duration(days: 1)));
    print(day2);
    day1 = formatter.format(now);
    print(day1);
  Hive.box('room').put('day1', day1);
    Hive.box('room').put('day2', day2);
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 3));
    animationSize = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 1.0, end: 1.5), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 1.5, end: 1.0), weight: 1)
    ]).animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.8, 1.0)));
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    setState(() {
      monthInLetterCheckIn = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(5));
    });
    setState(() {
      monthInLetterCheckOut = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(6));
    });

    if (Hive.box('adult').get(5) != null) {
      setState(() {
        nights = Hive.box('adult').get(5).toString();
      });
    } else {
      nights = '1';
    }
    if(nights == '1'){
      setState(() {
        night = 'night';
      });
    } else{
      setState(() {
        night = 'nights';
      });
    }
      setState(() {
        checkInDate =
            Hive.box('code').get(3).toString() + " ";
      });
      setState(() {
        checkOutDate =
            Hive.box('code').get(4).toString() + " ";
      });

    if (Hive.box('adult').get(1) !=null) {
      setState(() {
        rooms = Hive.box('adult').get(1);
      });
    } else {
      setState(() {
        rooms = 1;
      });
    }
    if(rooms == 1){
      setState(() {
        roomLabel = "Room";
      });
    } else{
      setState(() {
        roomLabel = "Rooms";
      });
    }
    if (Hive.box('adult').get(2) != null)  {
      adultArray = Hive.box('adult').get(2);
      setState(() {
        for(int i=0;i<adultArray.length;i++){
          adults=adults + adultArray[i];
        }
      });
    } else {
      setState(() {
        adults = 2;
      });
    }
    // if(adults==1){
    //   setState(() {
    //     adult = "Adult";
    //   });
    // } else {
    //   setState(() {
    //     adult="Adults";
    //   });
    // }
    if (Hive.box('adult').get(3) != null) {
      childrenArray = Hive.box('adult').get(3);

      setState(() {
        for(int i=0;i<childrenArray.length;i++){
          children = children + childrenArray[i];
        }
      });
    } else {
      setState(() {
        children = 0;
      });
    }
//     if(children==0||children==1){
// setState(() {
//   child="Child";
// });
//     } else {
// setState(() {
//   child = "Children";
// });
//     }

    super.initState();
  }

  checkInternetStatus(BuildContext context) async {
    bool result = await DataConnectionChecker().hasConnection;
    if(result == true) {
      if (Hive.box('lang').get(10) != null) {
        Platform.isIOS? Navigator.push(context,
            MaterialPageRoute(builder: (context) => WebViewClass())):Navigator.push(context,
            MaterialPageRoute(builder: (context) => Webview()));
      } else {
        final snackbar = SnackBar(
          content: Text(
            Constants.CHOOSE_DESTINATION,
            style: TextStyle(
                fontStyle: FontStyle.normal,
                fontFamily: 'Poppins',
                fontSize: 14.0),
          ),
          duration: Duration(seconds: 1),
          backgroundColor: Constants.kitGradients[0],
        );
        _globalKey.currentState.showSnackBar(snackbar);
      }
    } else {
      final snackbar = SnackBar(
        content: Text(
          Constants.INTERNET_CONNECTION_STATUS,
          style: TextStyle(
              fontStyle: FontStyle.normal,
              fontFamily: 'Poppins',
              fontSize: 14.0),
        ),
        duration: Duration(seconds: 1),
        backgroundColor: Constants.kitGradients[0],
      );
      _globalKey.currentState.showSnackBar(snackbar);
      print(DataConnectionChecker().lastTryResults);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _globalKey,
        backgroundColor: Constants.kitGradients[3],
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0), // here the desired height
            child: AppBar(
              elevation: 0,
              backgroundColor: Constants.kitGradients[0],
            )),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              child: Text(
                getTranslated(context, 'Search'),
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.w300,
                    fontStyle: FontStyle.normal),
              ),
              onTap: () {
                checkInternetStatus(context);

              },
            ),
            Transform.scale(
                scale: animationSize.value,
                child: MaterialButton(
                  disabledColor: Constants.kitGradients[7],
                  height: screenHeight(context, dividedBy: 8.8),
                  shape: CircleBorder(),
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.white,
                    size: 40,
                  ),
                  color: Constants.kitGradients[0],
                  onPressed: () {
                   checkInternetStatus(context);
                  },
                )),
          ],
        ),
        drawer: HomeDrawer(),
        body: Builder(
          builder: (context) => SafeArea(
            top: true,
            left: true,
            bottom: true,
            child: Stack(
              children: [
                Column(
                  children: [
                    Container(
                      height: screenHeight(context, dividedBy: 2.4),
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/bg.jpg"),
                            fit: BoxFit.cover),
                      ),
                      child: Container(
                        height: screenHeight(context, dividedBy: 12),
                        width: screenWidth(context, dividedBy: 9),
                        child: Center(
                          // child: Padding(
                          //   padding: EdgeInsets.symmetric(
                          //       vertical: screenWidth(context, dividedBy: 4),
                          //       horizontal:
                          //           screenWidth(context, dividedBy: 2.5)),
                          child: Container(
                            height: 60,
                            width: 60,
                            child: SvgPicture.asset(
                              "assets/images/app_icon_final.svg",
                              fit: BoxFit.cover,
                              color: Colors.white,
                            ),
                          ),
                          // ),
                        ),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Constants.kitGradients[7],
                                Constants.kitGradients[7].withOpacity(.95),
                                Constants.kitGradients[7].withOpacity(.90),
                                Constants.kitGradients[7].withOpacity(.80),
                                Constants.kitGradients[7].withOpacity(.70),
                                Constants.kitGradients[7].withOpacity(.50),
                                Constants.kitGradients[7].withOpacity(.40),
                                Constants.kitGradients[7].withOpacity(.30),
                                Constants.kitGradients[7].withOpacity(.20),
                                Constants.kitGradients[7].withOpacity(.10)
                              ]),
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  left: 10,
                  top: 10,
                  child: GestureDetector(
                    child: Container(
                      child: Icon(
                        Icons.dehaze,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                  ),
                ),
                Positioned(
                  top: screenHeight(context, dividedBy: 2.8),
                  child: Container(
                    height: screenHeight(context, dividedBy: 2),
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.only(topLeft: Radius.circular(40)),
                        color: Colors.white),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 15),
                            ),
                            Text(
                              getTranslated(context, 'Search'),
                              style: TextStyle(
                                  fontSize: 24.0,
                                  color: Constants.kitGradients[2],
                                  fontWeight: FontWeight.w300,
                                  fontFamily: 'Gilroy',
                                  fontStyle: FontStyle.normal),
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 80),
                            ),
                            Text(
                              getTranslated(context, 'Hotels'),
                              style: TextStyle(
                                  fontSize: 24.0,
                                  fontWeight: FontWeight.w800,
                                  fontFamily: 'Gilroy',
                                  color: Constants.kitGradients[7],
                                  fontStyle: FontStyle.normal),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        HomePageFieldBox(
                          assetPath: "assets/images/location3_icon.svg",
                          fieldVal: Hive.box('lang').get(10) != null
                              ? Hive.box('lang').get("place")+ (Hive.box('lang').get('country').toString().trim()!=''? ", "+Hive.box('lang').get('country'):"")
                              : getTranslated(context, 'select_destination'),
                          labelHead: getTranslated(context, 'Destination'),
                          onPressed: () {

                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DestinationPage()),
                            );
                          },
                        ),
                        // }),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        HomePageFieldBox(
                          assetPath: "assets/images/calander.svg",
                          fieldVal: (Hive.box('code').get(3) !=null ? checkInDate
                              +getTranslated(context, monthInLetterCheckIn)
                              :day1 ) +
                              "-" +
                              ( Hive.box('code').get(4) !=null ? checkOutDate
                              +getTranslated(context, monthInLetterCheckOut)
                              :day2)+"( "+ nights+" "+getTranslated(context, night)+" )",
                          labelHead: getTranslated(context, 'Dates'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DatePicker()),
                            );
                          },
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 80),
                        ),
                        HomePageFieldBox(
                          assetPath: "assets/images/user.svg",
                          fieldVal: rooms.toString()+" "+getTranslated(context, roomLabel)+", "+ counterValueAdultsSum.toString()+" " +getTranslated(context, adultLabel)+", " +
                              counterValueChildrenSum.toString()+" "+ getTranslated(context, childLabel),

                          labelHead: getTranslated(context, 'Guests'),
                          onPressed: () {
                            // _guestModalBottomSheet(context);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ModalBottomSheet()),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
