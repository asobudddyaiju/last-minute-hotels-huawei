
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:last_minute_hotels_huawei/localization/locale.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/home_page.dart';
import 'package:last_minute_hotels_huawei/src/utils/constants.dart';
import 'package:last_minute_hotels_huawei/src/utils/object_factory.dart';
import 'package:last_minute_hotels_huawei/src/utils/utils.dart';


class Login extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _LoginState();

  }
}

class _LoginState extends State<Login> {
  bool _isLoading;
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    _isLoading = false;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          child: AppBar(
            leading: new IconButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                        (route) => false);
              },
              icon: new Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
            ),
            backgroundColor: Constants.kitGradients[13],
            elevation: 0.5,
          )),
        key: _globalKey,
        body: Container(
          width: screenWidth(context,dividedBy: 1),
          height: screenHeight(context,dividedBy: 1),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color(0xFF15548F),Colors.white,Colors.white,Colors.white, Colors.white],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter),
          ),
          child: SafeArea(
          child: Center(
            child: Stack(
              children: [
                Column(
                  children: [
                    Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.4,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [ Constants.kitGradients[13], Colors.white],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: screenHeight(context,dividedBy: 6.0),horizontal: screenWidth(context,dividedBy: 2.5 )),
                          child:SvgPicture.asset(
                            "assets/images/app_icon_final.svg",
                            fit: BoxFit.fill,color: Colors.white,
                          ),),
                      ),
                    ),
                    Center(
                      child: Text(
                        getTranslated(context, 'Sign_In_and_Save'),
                        style: TextStyle(
                            fontFamily: 'NexaBold',
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF484848),
                            fontSize: 18.0,
                            fontStyle: FontStyle.normal),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 10.0, bottom: 10.0, left: 20.0, right: 20.0),
                      child: Center(
                        child: Text(
                          getTranslated(context, 'Track_prices,_organize_travel_plans_and_access_member-only_with_your_HotelBooking_account'),
                          style: TextStyle(
                              fontFamily: 'NexaLight',
                              fontWeight: FontWeight.w300,
                              color: Color(0xFF7E7E7E),
                              fontSize: 10.0,
                              fontStyle: FontStyle.normal),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(left: 35.0, right: 35.0, top: 20.0),
                      child: Center(
                        child: GestureDetector(
                          onTap: () async {

                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60.0,
                            decoration: BoxDecoration(
                                border: Border.all(style: BorderStyle.none),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFDBE7EB).withOpacity(0.2),
                                      offset: Offset(0, 5),
                                      blurRadius: 1.0,
                                      spreadRadius: 1.0)
                                ]),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/google_icon.png',
                                      width: 20.0, height: 20.0),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      getTranslated(context, 'Continue_with_Google'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300,
                                          fontStyle: FontStyle.normal,
                                          color: Color(0xFF484848)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(left: 35.0, right: 35.0, top: 20.0),
                      child: Center(
                        child: GestureDetector(
                          onTap: () async {

                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60.0,
                            decoration: BoxDecoration(
                                border: Border.all(style: BorderStyle.none),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFDBE7EB).withOpacity(0.2),
                                      offset: Offset(0, 5),
                                      spreadRadius: 1.0,
                                      blurRadius: 1.0)
                                ]),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/facebook_icon.png', width: 20.0, height: 20.0),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      getTranslated(context, 'Continue_with_Facebook'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300,
                                          fontStyle: FontStyle.normal,
                                          color: Color(0xFF484848)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SafeArea(
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: 50.0, bottom: 0.0, left: 15.0, right: 15.0),
                          child: RichText(
                            text: TextSpan(
                                text:getTranslated(context, 'By_signing_up_you_agree_to_our'),
                                style: TextStyle(
                                  fontFamily: 'NexaLight',
                                  fontWeight: FontWeight.w300,
                                  color: Color(0xFF404040),
                                  fontSize: 10.0,
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                      recognizer: TapGestureRecognizer()..onTap = () => print('you just clicked terms and conditions'),
                                      text: getTranslated(context, 'Terms_and_conditions'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontWeight: FontWeight.w300,
                                          color: Colors.blue,
                                          fontSize: 10.0)),
                                  TextSpan(
                                      text: ' '+ getTranslated(context,'And'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontWeight: FontWeight.w300,
                                          fontSize: 10.0,
                                          color: Color(0xFF404040))),
                                  TextSpan(
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () => print(
                                            'you just clicked Privacy Policy'),
                                      text: ' '+getTranslated(context, 'Privacy_policy'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontWeight: FontWeight.w300,
                                          color: Colors.blue,
                                          fontSize: 10.0))
                                ]),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                _isLoading == true
                    ? Center(
                  child: Container(
                      height: 40.0,
                      width: 40.0,
                      child: Center(child: CircularProgressIndicator())),
                )
                    : Stack()
              ],
            ),
          ),
        ),),
      );

  }
  void _showSnackbar(String snackdata) {
    final snack = SnackBar(
      content: Text(snackdata,style: TextStyle(fontFamily: "Poppins",fontStyle: FontStyle.normal,fontSize: 12.0),),
      duration: Duration(seconds: 2),
      backgroundColor: Constants.kitGradients[0],
    );
    _globalKey.currentState.showSnackBar(snack);
  }



}
