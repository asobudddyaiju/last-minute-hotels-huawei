// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'age_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AgeAdapter extends TypeAdapter<Age> {
  @override
  final int typeId = 0;

  @override
  Age read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Age(
      age: (fields[0] as List)
          ?.map((dynamic e) => (e as List)?.cast<String>())
          ?.toList(),
    );
  }

  @override
  void write(BinaryWriter writer, Age obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.age);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AgeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
