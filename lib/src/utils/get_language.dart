class GetLanguage {
  List<String> currencyList = [
    "AED",
    "BGN",
    "BRL",
    "CNY",
    "CZK",
    "HRK",
    "DKK",
    "EUR",
    "HUF",
    "HKD",
    "ISK",
    "INR",
    "IDR",
    "ILS",
    "JPY",
    "KRW",
    "LTL",
    "MYR",
    "NOK",
    "PHP",
    "PLN",
    "RON",
    "RUB",
    "RSD",
    "SEK",
    "THB",
    "TRY",
    "UAH",
    "USD",
    "VND",
  ];
  List<String> countries = [
    "العربية",
    "Български",
    "Català",
    "简体中文",
    "Čeština",
    "Hrvatski",
    "Dansk",
    "Eesti",
    "Suomi",
    "Français",
    "Deutsch",
    "Ελληνικά",
    "Magyar",
    "Íslenska",
    "Bahasa Indonesia",
    "עברית",
    "Italiano",
    "日本語",
    "한국어",
    "Latviski",
    "Lietuvių",
    "Bahasa Malaysia",
    "Nederlands",
    "Norsk",
    "Filipino",
    "Polski",
    "Português",
    "Română",
    "Русский",
    "Srpski",
    "Slovenčina",
    "Slovenščina",
    "Español",
    "Svenska",
    "ภาษาไทย",
    "Türkçe",
    "Українська",
    "English",
    "Tiếng Việt",
  ];

  List<String> locales = [
    "ar",
    "bg",
    "zh",
    "cz",
    "hr",
    "da",
    "es",
    "et",
    "fi",
    "fr",
    "de",
    "el",
    "hu",
    "is",
    "id",
    "he",
    "it",
    "ja",
    "ko",
    "lv",
    "lt",
    "ms",
    "nl",
    "no",
    "tl",
    "pl",
    "pt",
    "ro",
    "ru",
    "sr",
    "sk",
    "sl",
    "sv",
    "th",
    "tr",
    "uk",
    "en",
    "vi"
  ];
  List<String> dateLocale = [
    "ar",
    "bg",
    "zh_CN",
    "cs",
    "hr",
    "da",
    "es",
    "et",
    "fi",
    "fr",
    "de",
    "el",
    "hu",
    "is",
    "id",
    "he",
    "it",
    "ja",
    "ko",
    "lv",
    "lt",
    "ms",
    "nl",
    "no",
    "tl",
    "pl",
    "pt",
    "ro",
    "ru",
    "sr",
    "sk",
    "sl",
    "sv",
    "th",
    "tr",
    "uk",
    "en_US",
    "vi"
  ];

  List<String> getCountryLanguage(String country) {
    String currency;
    String language;
    String locale;
    String date;
    switch (country) {
      case "ar_AE":
        {
          currency = currencyList[0];
          language = countries[0];
          locale = locales[0];
          date = dateLocale[0];

          break;
        }
      case "bg_BG":
        {
          currency = currencyList[1];
          language = countries[1];
          locale = locales[1];
          date = dateLocale[1];



          break;
        }
      // case "Brazil":
      //   {
      //     currency = currencyList[2];
      //
      //     break;
      //   }
      case "zh_CN":
        {
          currency = currencyList[3];
          language = countries[3];
          locale = locales[2];
          date = dateLocale[2];

          break;
        }
      case "cs_CZ":
        {
          currency = currencyList[4];
          language = countries[4];
          locale = locales[3];
          date = dateLocale[3];

          break;
        }
      case "hr_HR":
        {
          currency = currencyList[5];
          language = countries[5];
          locale = locales[4];
          date = dateLocale[4];

          break;
        }
      case "da_DK":
        {
          currency = currencyList[6];
          language = countries[6];
          locale = locales[5];
          date = dateLocale[5];


          break;
        }
      case "es_ES":
        {
          currency = currencyList[7];
          language = countries[32];
          locale = locales[6];
          date = dateLocale[6];

          break;
        }
      case "et_EE":
        {
          currency = currencyList[7];
          language = countries[7];
          locale = locales[7];
          date = dateLocale[7];

          break;
        }
      case "fi_FI":
        {
          currency = currencyList[7];
          language = countries[8];
          locale = locales[8];
          date = dateLocale[8];

          break;
        }
      case "fr_FR":
        {
          currency = currencyList[7];
          language = countries[9];
          locale = locales[9];
          date = dateLocale[9];

          break;
        }
      case "de_DE":
        {
          currency = currencyList[7];
          language = countries[10];
          locale = locales[10];
          date = dateLocale[10];

          break;
        }
      case "el_GR":
        {
          currency = currencyList[7];
          language = countries[11];
          locale = locales[11];
          date = dateLocale[11];

          break;
        }
      case "hu_HU":
        {
          currency = currencyList[8];
          language = countries[12];
          locale = locales[12];
          date = dateLocale[12];

          break;
        }
      case "is_IS":
        {
          currency = currencyList[10];
          language = countries[13];
          locale = locales[13];
          date = dateLocale[13];

          break;
        }
      // case "India":
      //   {
      //     currency = currencyList[11];
      //
      //     break;
      //   }
      case "id_ID":
        {
          currency = currencyList[12];
          language = countries[14];
          locale = locales[14];
          date = dateLocale[14];

          break;
        }
      case "he_IL":
        {
          currency = currencyList[13];
          language = countries[15];
          locale = locales[15];
          date = dateLocale[15];

          break;
        }
      case "it_IT":
        {
          currency = currencyList[7];
          language = countries[16];
          locale = locales[16];
          date = dateLocale[16];

          break;
        }
      case "ja_JP":
        {
          currency = currencyList[14];
          language = countries[17];
          locale = locales[17];
          date = dateLocale[17];

          break;
        }
      case "ko_KO":
        {
          currency = currencyList[15];
          language = countries[18];
          locale = locales[18];
          date = dateLocale[18];

          break;
        }
      case "lv_LV":
        {
          currency = currencyList[7];
          language = countries[19];
          locale = locales[19];
          date = dateLocale[19];

          break;
        }
      case "lt_LT":
        {
          currency = currencyList[16];
          language = countries[20];
          locale = locales[20];
          date = dateLocale[20];

          break;
        }
      case "ms_MY":
        {
          currency = currencyList[17];
          language = countries[21];
          locale = locales[21];
          date = dateLocale[21];

          break;
        }
      case "nl_NL":
        {
          currency = currencyList[7];
          language = countries[22];
          locale = locales[22];
          date = dateLocale[22];

          break;
        }
      case "no_NO":
        {
          currency = currencyList[18];
          language = countries[23];
          locale = locales[23];
          date = dateLocale[23];

          break;
        }
      case "tl_PH":
        {
          currency = currencyList[19];
          language = countries[24];
          locale = locales[24];
          date = dateLocale[24];

          break;
        }
      case "pl_PL":
        {
          currency = currencyList[20];
          language = countries[25];
          locale = locales[25];
          date = dateLocale[25];

          break;
        }
      case "pt_PT":
        {
          currency = currencyList[7];
          language = countries[26];
          locale = locales[26];
          date = dateLocale[26];

          break;
        }
      case "ro_RO":
        {
          currency = currencyList[21];
          language = countries[27];
          locale = locales[27];
          date = dateLocale[27];

          break;
        }
      case "ru_RU":
        {
          currency = currencyList[22];
          language = countries[28];
          locale = locales[28];
          date = dateLocale[28];

          break;
        }
      case "sr_SP":
        {
          currency = currencyList[23];
          language = countries[29];
          locale = locales[29];
          date = dateLocale[29];

          break;
        }
      case "sk_SK":
        {
          currency = currencyList[7];
          language = countries[30];
          locale = locales[30];
          date = dateLocale[30];

          break;
        }
      case "sl_SL":
        {
          currency = currencyList[7];
          language = countries[31];
          locale = locales[31];
          date = dateLocale[31];

          break;
        }
      case "sv_SV":
        {
          currency = currencyList[24];
          language = countries[33];
          locale = locales[32];
          date = dateLocale[32];

          break;
        }
      case "th_TH":
        {
          currency = currencyList[25];
          language = countries[34];
          locale = locales[33];
          date = dateLocale[33];

          break;
        }
      case "tr_TR":
        {
          currency = currencyList[26];
          language = countries[35];
          locale = locales[34];
          date = dateLocale[34];

          break;
        }
      case "uk_UK":
        {
          currency = currencyList[27];
          language = countries[36];
          locale = locales[35];
          date = dateLocale[35];

          break;
        }
      case "en_US":
        {
          currency = currencyList[28];
          language = countries[37];
          locale = locales[36];
          date = dateLocale[36];

          break;
        }
      case "vi_VN":
        {
          currency = currencyList[29];
          language = countries[38];
          locale = locales[37];
          date = dateLocale[37];

          break;
        }
      default :
          currency = currencyList[28];
          language = countries[37];
          locale = locales[36];
          date = dateLocale[36];

          break;
    }
    return [currency,language,locale,date];
  }
}
