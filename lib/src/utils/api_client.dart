import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:last_minute_hotels_huawei/src/utils/constants.dart';
import 'package:http/http.dart' as http;

class ApiClient {
  ApiClient() {
    initClient();
  }

//for api client testing only
  ApiClient.test({@required this.dio});

  Dio dio;
  BaseOptions _baseOptions;

  initClient() async {
    _baseOptions = new BaseOptions(
        baseUrl: Constants.baseUrl,
        connectTimeout: 30000,
        receiveTimeout: 1000000,
        followRedirects: true,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.acceptHeader: 'application/json',
        },
        responseType: ResponseType.json,
        receiveDataWhenStatusError: true);

    dio = Dio(_baseOptions);

    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) {
        return true;
      };
    };

    dio.interceptors.add(CookieManager(new CookieJar()));
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions reqOptions) {
        return reqOptions;
      },
      onError: (DioError dioError) {
        return dioError.response;
      },
    ));
  }

  /// Verify User
  Future<Response> sampleApiCall() {
    return dio.post("", data: {});
  }

  /// feature list
  Future<Response> destinationList({String searchKey,String lang,myFunction})async {
     //String lang = Hive.box('lang').get(1)!=null?Hive.box('lang').get(1):"EN";
    final response = await http
        .get("http://book.hotelmost.com/AutoUniversal.ashx?search=$searchKey&languageCode=fr&callback=$myFunction",
      headers: {
        'Content-type' : 'application/json',
        'Accept': 'application/json'
      },
    );
    return dio.get(
         "http://book.hotelmost.com/AutoUniversal.ashx?search=$searchKey&languageCode=fr&callback=$myFunction"
      // "http://brands.datahc.com/AutoUniversal.ashx?search=$searchKey&languageCode=fr&callback=myFunction"
    );


  }
  Future<void> fetchHttpGet(
      { Function callback,String searchKey,myFunction}) async {
    final response = await http
        .get("http://book.hotelmost.com/AutoUniversal.ashx?search=$searchKey&languageCode=fr&callback=$myFunction",
      headers: {
        'Content-type' : 'application/json',
        'Accept': 'application/json'
      },
    );

    if(response == null || response.statusCode != 200) {
      callback(null);
    } else {
      final responseJson = json.decode(response.body);
      callback(responseJson);
    }
  }

  /// feature list
  Future<Response> trendingList() {
    return dio.get("");
  }

  /// search Flatmate
//  Future<Response> searchFlatmate(String searchText) {
//    dio.options.headers
//        .addAll({"Authorization": ObjectFactory().prefs.getAuthToken()});
//    return dio.get(
//        Urls.SEARCH_FLATMATE + "{\"search_param\"= \"$searchText\"}");
//  }

}
