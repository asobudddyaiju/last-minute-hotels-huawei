import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:last_minute_hotels_huawei/localization/locale.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/currency.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/home_page.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/region.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/about_us.dart';
import 'package:last_minute_hotels_huawei/src/ui/widgets/build_dropcurrency.dart';
import 'package:last_minute_hotels_huawei/src/ui/widgets/build_dropdown.dart';
import 'package:last_minute_hotels_huawei/src/ui/widgets/profile_list_tile.dart';
import 'package:last_minute_hotels_huawei/src/utils/object_factory.dart';
import 'package:last_minute_hotels_huawei/src/utils/utils.dart';

import 'login.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String defaultLang = Platform.localeName;
  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          // here the desired height
          child: AppBar(
            leading: new IconButton(
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (route) => false);
              },
            ),
            title: Text(
              getTranslated(context, 'Profile'),
              style: TextStyle(
                  color: Colors.black54, fontWeight: FontWeight.normal),
            ),
            elevation: 0,
            backgroundColor: Colors.white,
          )),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
          Card(
            elevation: 0.5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 15),
                          right: screenWidth(context, dividedBy: 15),
                          top: screenHeight(context, dividedBy: 75)),
                      child: Text(
                        getTranslated(context, 'language'),
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 75),
                          right: screenWidth(context, dividedBy: 50),
                          left: screenWidth(context, dividedBy: 50)),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegionPage()));
                        },
                        child: Text(
                          Hive.box('lang').get(2) != null
                              ? Hive.box('lang').get(2)
                              : ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[1],
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 15),
                          right:screenWidth(context, dividedBy: 15),
                          top: screenHeight(context, dividedBy: 100)),
                      child: Text(
                        getTranslated(context, 'Currency'),
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 75),
                            right: screenWidth(context, dividedBy: 50),
                            left: screenWidth(context, dividedBy: 50),
                            bottom: screenHeight(context, dividedBy: 75)),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CurrencyPage()));
                          },
                          child: Text(
                            Hive.box('code').get(2) != null
                                ? Hive.box('code').get(2).toString().split(" ").first
                                : Hive.box('room').get('country')!=null?ObjectFactory().getCurrency.getCurrency(Hive.box('room').get('currency')):"USD",
                            overflow: TextOverflow.visible,
                            style: TextStyle(fontSize: 14),
                          ),
                        )),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: screenHeight(context, dividedBy: 15)),
          Card(
            elevation: 0.5,
            child: Column(children: [
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AboutPage()));
                },
                child: ProfileListTile(
                  listText: getTranslated(context, 'About_Us'),
                  listText1: '',
                ),
              ),
              ProfileListTile(
                listText: getTranslated(context, 'Help_and_feedback'),
                listText1: '',
              ),
              ProfileListTile(
                listText: getTranslated(context, 'Terms_and_conditions'),
                listText1: '',
              ),
              ProfileListTile(
                listText: getTranslated(context, 'Privacy_policy'),
                listText1: '',
              ),
              GestureDetector(
                onTap: () {

                },
                child: ProfileListTile(
                  listText: getTranslated(context, ""),
                  listText1: '',
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}
