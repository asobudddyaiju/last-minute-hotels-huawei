import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class DropCurrency extends StatefulWidget {
  @override
  _DropCurrencyState createState() => _DropCurrencyState();
}

String dropdownvalue;

class _DropCurrencyState extends State<DropCurrency> {
  @override
  void initState() {
    super.initState();
    setState(() {
      dropdownvalue = Hive.box('code').get(2);
    });
  }

  List<String> currencies = [
    "AED",
    "BGN",
    "BRL",
    "CNY",
    "CZK",
    "HRK",
    "DKK",
    "EUR",
    "HUF",
    "HKD",
    "ISK",
    "IDR",
    "ILS",
    "JPY",
    "KRW",
    "LTL",
    "MYR",
    "NOK",
    "PHP",
    "PLN",
    "RON",
    "RUB",
    "RSD",
    "SEK",
    "THB",
    "TRY",
    "UAH",
    "USD",
    "VND",
  ];

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: Container(
        height: 10,
        child: DropdownButton<String>(
          iconSize: 0.0,
          value: dropdownvalue,
          style: TextStyle(fontSize: 9.0, color: Colors.blue),
          items: currencies
              .map<DropdownMenuItem<String>>((String value) =>
                  DropdownMenuItem<String>(value: value, child: Text(value)))
              .toList(),
          onChanged: (currency) {
            setState(() {
              dropdownvalue = currency;
            });
            Hive.box('code').put(2, currency);
          },
          hint: Text(
            'USD',
            style: TextStyle(fontSize: 9.0, color: Colors.blue),
          ),
        ),
      ),
    );
  }
}
