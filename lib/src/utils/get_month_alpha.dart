class GetMonthAlpha {
  List<String> monthslist = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  String getCheckInMonth(String mon) {
    String month;
    switch (mon) {
      case "1":
        {
          month = monthslist[0];
          break;
        }
      case "2":
        {
          month = monthslist[1];

          break;
        }
      case "3":
        {
          month = monthslist[2];

          break;
        }
      case "4":
        {
          month = monthslist[3];

          break;
        }
      case "5":
        {
          month = monthslist[4];
      
          break;
        }
      case "6":
        {
          month = monthslist[5];

          break;
        }
      case "7":
        {
          month = monthslist[6];

          break;
        }
      case "8":
        {
          month = monthslist[7];

          break;
        }
      case "9":
        {
          month = monthslist[8];

          break;
        }
      case "10":
        {
          month = monthslist[9];

          break;
        }
      case "11":
        {
          month = monthslist[10];

          break;
        }
      case "12":
        {
          month = monthslist[11];

          break;
        }
    }
    return month;
  }
}
