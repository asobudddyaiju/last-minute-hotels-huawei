import 'package:flutter/material.dart';

class DListTile extends StatefulWidget {
  final String listText;
  final IconData iconData;

  const DListTile({Key key, this.listText, this.iconData}) : super(key: key);

  @override
  _DListTileState createState() => _DListTileState();
}

class _DListTileState extends State<DListTile> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      contentPadding: EdgeInsets.only(left: 10.0, right: 0.0),
      leading: Icon(
        widget.iconData,
      ),
      title: Text(
        widget.listText,
        style: TextStyle(color: Colors.black),
      ),
    );
  }
}
