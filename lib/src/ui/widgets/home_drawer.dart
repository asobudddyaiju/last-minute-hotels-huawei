// import 'package:country_codes/country_codes.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:last_minute_hotels_huawei/localization/locale.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/currency.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/destination_page.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/login.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/profile_page.dart';
import 'package:last_minute_hotels_huawei/src/ui/screens/region.dart';
import 'package:last_minute_hotels_huawei/src/utils/constants.dart';
import 'package:last_minute_hotels_huawei/src/utils/object_factory.dart';
import 'package:last_minute_hotels_huawei/src/utils/utils.dart';

class HomeDrawer extends StatefulWidget {

  @override
  // _HomeDrawerState createState() => _HomeDrawerState();
  State<StatefulWidget> createState() {
    return _HomeDrawerState();
  }
}

class _HomeDrawerState extends State<HomeDrawer> {
// CountryDetails details = CountryCodes.detailsForLocale();
  String defaultLang = Platform.localeName;



  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          // Padding(
          //   padding: EdgeInsets.symmetric(horizontal: 15.0),
          //   child: Row(
          //     children: [
          //       Container(
          //         width: 38.0,
          //         height: 40.24,
          //         decoration: BoxDecoration(
          //           color: Colors.black.withOpacity(.05),
          //           shape: BoxShape.circle,
          //         ),
          //       ),
          //       Padding(
          //         padding: EdgeInsets.symmetric(horizontal: 15.0),
          //         child: Text("",
          //             style: TextStyle(
          //                 color: Color(0xFF272727),
          //                 fontWeight: FontWeight.w300,
          //                 fontSize: 18.0,
          //                 fontStyle: FontStyle.normal,
          //                 fontFamily: 'NexaLight')),
          //       ),
          //     ],
          //   ),
          // ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
          //   child: MaterialButton(
          //     color: Constants.kitGradients[0],
          //     onPressed: () {
          //
          //     },
          //     minWidth: 10,
          //     height: 40,
          //     shape: RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(30)),
          //     child: Text(
          //      "",
          //       style: TextStyle(fontSize: 18, color: Colors.white),
          //     ),
          //   ),
          // ),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DestinationPage()));
              },
              child: ListTile(
                leading: SvgPicture.asset("assets/images/search_icon.svg"),
                title: Text(
                  getTranslated(context, 'Search'),
                  style: TextStyle(
                      color: Colors.black.withOpacity(.50),
                      fontWeight: FontWeight.w200),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {

              },
              child: ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.black.withOpacity(.60),
                ),
                title: Text(
                  getTranslated(context, 'Profile'),
                  style: TextStyle(
                      color: Colors.black.withOpacity(.60),
                      fontWeight: FontWeight.w200),
                ),
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 4.7),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap:  () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RegionPage()));
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 100),
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Text(
                    getTranslated(context, 'language'),
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(
                      // vertical: screenHeight(context, dividedBy: 100),
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegionPage()));
                      },
                      child: Text(
                        Hive.box('lang').get(2) != null
                            ? Hive.box('lang').get(2)
                            :ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[1]
                        ,
                        // details.name,
                        style: TextStyle(fontSize: 12),
                      )))
            ],
          ),
          SizedBox(height: screenHeight(context,dividedBy: 75),),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CurrencyPage()));
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 100),
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Text(
                    getTranslated(context, 'Currency'),
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    // vertical: screenHeight(context, dividedBy: 100),
                    horizontal: screenWidth(context, dividedBy: 20)),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CurrencyPage()));
                    },
                    child: Container(
                      child: Flexible(
                        child: Text(
                            Hive.box('code').get(2) != null
                                ? Hive.box('code').get(2).toString().split(" ").first
                                :Hive.box('room').get('country')!=null?ObjectFactory().getCurrency.getCurrency(Hive.box('room').get('currency')):"USD",
                            overflow: TextOverflow.visible,
                            style: TextStyle(fontSize: 12)),
                      ),
                    )),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
